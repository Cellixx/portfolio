import React from "react";
import Card from "react-bootstrap/Card";
import { ImPointRight } from "react-icons/im";

function AboutCard() {
  return (
    <Card className="quote-card-view">
      <Card.Body>
        <blockquote className="blockquote mb-0">
          <p style={{ textAlign: "justify" }}>
            Howdy! Meu nome é <span className="purple">Cellixx</span>!
            <br />Sou de uma pequena cidade do Rio Grande do Sul, mas pequena mesmo!
            <br />
            <br />
            Tirando programação, existe mais coisas que eu gosto de fazer!
          </p>
          <ul>
            <li className="about-activity">
              <ImPointRight /> Jogar Fortnite
            </li>
            <li className="about-activity">
              <ImPointRight /> Escrever documentos
            </li>
            <li className="about-activity">
              <ImPointRight /> Sair com os amigos (apesar de eu não ter nenhum)
            </li>
          </ul>

          <p style={{ marginBlockEnd: 0, color: "rgb(155 126 172)" }}>
            "make the difference!"{" "}
          </p>
        </blockquote>
      </Card.Body>
    </Card>
  );
}

export default AboutCard;
