import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import myImg from "../../Assets/avatar.svg";
import Tilt from "react-parallax-tilt";
import {
  AiFillGithub,
  AiOutlineTwitter,
  AiFillInstagram,
} from "react-icons/ai";
import { FaLinkedinIn } from "react-icons/fa";

function Home2() {
  return (
    <Container fluid className="home-about-section" id="about">
      <Container>
        <Row>
          <Col md={8} className="home-about-description">
            <h1 style={{ fontSize: "2.6em" }}>
              DEIXE EU ME <span className="purple"> APRESENTAR </span>
            </h1>
            <p className="home-about-body">
            Olá! Meu nome é Cellixx1000 (é claro que é um nome inventado) e sou apaixonado por programação!
              <br />
              <br />Gosto muito de programar em
              <i>
                <b className="purple"> C++, C#, Javascript e Vue. </b>
              </i>
              <br />
              <br />
              Eu gosto muito de usar linux! Estou usando a 1 mês e estou gostando muito! Estou usando
              <i>
                <b className="purple"> EndeavourOS </b>com gnome!
              </i>
              <br />
              <br />
              Dica: Não seja igual aos outros, apenas seja você mesmo!
              <b className="purple"> Be yourself</b>!

              &nbsp;
            </p>
          </Col>
          <Col md={4} className="myAvtar">
            <Tilt>
              <img src={myImg} className="img-fluid" alt="avatar" />
            </Tilt>
          </Col>
        </Row>
        <Row>
          <Col md={12} className="home-about-social">
            <h1>Me chama aí!</h1>
            <p>
              Alguns contatos diretos <span className="purple">comigo </span>
            </p>
            <ul className="home-about-social-links">
              <li className="social-icons">
                <a
                  href="https://github.com/Cellixx1000"
                  target="_blank"
                  rel="noreferrer"
                  className="icon-colour  home-social-icons"
                >
                  <AiFillGithub />
                </a>
              </li>
            </ul>
          </Col>
        </Row>
      </Container>
    </Container>
  );
}
export default Home2;
