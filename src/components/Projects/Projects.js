import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProjectCard from "./ProjectCards";
import Particle from "../Particle";

import leaf from "../../Assets/Projects/leaf.png";
import emotion from "../../Assets/Projects/emotion.jpeg";
import editor from "../../Assets/Projects/codeEditor.png";
import twinst from "../../Assets/Projects/twinst.png";
import suicide from "../../Assets/Projects/suicide.png";
import bitsOfCode from "../../Assets/Projects/blog.png";

function Projects() {
  return (
    <Container fluid className="project-section">
      <Particle />
      <Container>
        <h1 className="project-heading">
          Alguns dos meus recentes <strong className="purple">trabalhos </strong>
        </h1>
        <p style={{ color: "white" }}>
        </p>
        <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={twinst}
              isBlog={false}
              title="Twinst"
              description="Um simples e moderno BOT de economia e moderação para discord!"
              link="https://discord.gg/BtZRvzCRgB"
            />
          </Col>

        
        </Row>
      </Container>
    </Container>
  );
}

export default Projects;
